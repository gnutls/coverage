all: stamp_coverage stamp_cyclo

DEFAULT_TAG=master

stamp_coverage:
	rm -rf gnutls-git
	git clone --depth 2 --branch $(DEFAULT_TAG) https://gitlab.com/gnutls/gnutls.git gnutls-git
	cd gnutls-git && \
	git submodule update --init && ./bootstrap && \
	CFLAGS="-g -Og" ./configure --disable-guile --disable-padlock --enable-code-coverage --disable-manpages --disable-gcc-warnings --disable-maintainer-mode --disable-doc && \
	make -j$$(nproc) && \
	make -C fuzz check && \
	make V=1 CODE_COVERAGE_OUTPUT_DIRECTORY=../public/master-fuzz code-coverage-capture && \
	make -C gl/tests check && \
	make -C tests check && \
	make V=1 CODE_COVERAGE_OUTPUT_DIRECTORY=../public/master code-coverage-capture
	cp main.html public/index.html
	mkdir -p public/master-fuzz/
	contrib/make-coverage-badge "fuzz-coverage" public/master-fuzz/index.html
	mv badge.svg public/master-fuzz
	contrib/make-coverage-badge "coverage" public/master/index.html && \
	mv badge.svg public/master
	cat public/master/index.html|grep headerCovTableEntry|grep '%'|head -1|sed 's/^.*>\([0-9]\+\.[0-9]\+\s*%\)<.*$$/ coverage lines: \1/'
	touch $@

stamp_coverity:
	rm -rf gnutls-git
	git clone --branch $(DEFAULT_TAG) https://gitlab.com/gnutls/gnutls.git gnutls-git
	cd gnutls-git && \
	git submodule update --init && \
	wget https://scan.coverity.com/download/linux64 --post-data "token=${COVERITY_SCAN_TOKEN}&project=${COVERITY_SCAN_PROJECT_NAME}" -O /tmp/coverity_tool.tgz && \
	tar xfz /tmp/coverity_tool.tgz && \
	./bootstrap && \
	CFLAGS="-g -Og" dash ./configure --disable-gcc-warnings --disable-guile --disable-maintainer-mode --disable-doc && \
	cov-analysis-linux64-*/bin/cov-build --dir cov-int make -j$(nproc) && \
	tar cfz cov-int.tar.gz cov-int && \
	curl https://scan.coverity.com/builds?project=${COVERITY_SCAN_PROJECT_NAME} \
	    --form token=${COVERITY_SCAN_TOKEN} --form email=n.mavrogiannopoulos@gmail.com \
	    --form file=@cov-int.tar.gz --form version="`git describe --tags`" \
	    --form description="CI build"
	touch $@

stamp_cyclo: stamp_coverage
	(cd gnutls-git/lib && \
		pmccabe `find . -type f -name \*.[ch] \
				| grep -Ev '(/minitasn1/|/unistring/)' | sed 's,^./,,'` \
		| sort -nr \
		| LANG=C awk -f ../build-aux/pmccabe2html \
			-v lang=html -v name="$(PACKAGE_STRING)" \
			-v vcurl=$(vcurl) \
			-v url="https://www.gnutls.org" \
			-v css=../build-aux/pmccabe.css) \
			> cyclo.html
	mv cyclo.html public/master/
	touch $@

clean:
	rm -f *~ stamp_coverage stamp_cyclo stamp_coverity
	rm -rf gnutls-git
